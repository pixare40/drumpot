import * as React from 'react';
import { StyleSheet, FlatList, View, ListRenderItemInfo, Text } from 'react-native';
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel');
import ArtistListItemComponent = require('../artist/ArtistListItemComponent');
import TrackFiendStyles = require('../../TrackFiendStyles');
import TopFollowedArtistsComponent = require('../artist/TopFollowedArtistsComponent');
import { Container, Content, Input, Form, Item } from 'native-base';
import { NavigationScreenConfigProps } from 'react-navigation';

interface ArtistSearchComponentState{
    artists: ArtistInfoModel[]
}

const _styles = StyleSheet.create({
    rootViewStyles: {
        paddingLeft: 5,
        paddingRight: 5,
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    },

    searchInputFieldStyles: {
        borderStyle: 'solid',
        borderBottomWidth: 1,
        borderColor: TrackFiendStyles.color.mainAppColor,
        height: 44,
        margin: 2,
        textAlign: 'justify',
        borderRadius: 0
    },

    scrollViewStyles: {
        backgroundColor: TrackFiendStyles.color.listBackgroundColour
    }
});

class ArtistSearchComponent extends React.Component<{}, ArtistSearchComponentState>{

    render(): JSX.Element | null{
        return(
            <Text>
                {"Artist Search COmponent"}
            </Text>
            // <View style= { _styles.rootViewStyles }>
            //     <View>
            //         <Form>
            //             <Item>
            //                 <Input placeholder={ 'Search' } 
            //                     onChangeText= { (searchText) =>{ this.fetchSearchResults(searchText) }} />
            //             </Item>
            //         </Form>
            //     </View>
            //     { this.renderSearchResultsOrTopFollowed() }
            // </View>
        )
    }

    renderSearchResultsOrTopFollowed = ()=>{
        let myProps = this.props as NavigationScreenConfigProps;
        if(this.state.artists.length == 0){
            return (
                <TopFollowedArtistsComponent navigationProps={ myProps }/>
            )
        }
        else{
            return (
                <FlatList data={ this.state.artists }
                                        renderItem={ this.renderSearchResults }
                                        style= { _styles.scrollViewStyles }/>
            )
        }
        
    }
    
    renderSearchResults = (item: ListRenderItemInfo<ArtistInfoModel>) => {
        return (
                <ArtistListItemComponent key={ item.item.name } 
                                    artist={ item.item } 
                                    onArtistTapped = { this.goToArtist }/>
        )
    }

    fetchSearchResults = (searchText: string) =>{
        if(!searchText){
            return;
        }
        
       // ArtistSearchStore.fetchArtistSearchResults(searchText);
    }

    goToArtist = (artistId: number) =>{
        let myProps = this.props as NavigationScreenConfigProps;
        //ArtistStore.setCurrentViewedArtist(artistId)
        myProps.navigation.navigate('Artist', {artist_id: artistId})
    }
}

export = ArtistSearchComponent;