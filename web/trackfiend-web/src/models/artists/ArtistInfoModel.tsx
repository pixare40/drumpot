import NewsItem = require ('../NewsItem')

interface ArtistInfoModel{
    id: number,
    name: string,
    realname: string,
    cname: string,
    followers: number,
    artist_image_url: string,
    news: NewsItem[]
}

export = ArtistInfoModel;