import * as React from 'react';
import { TabNavigator, StackNavigator, NavigationContainer } from 'react-navigation';
import { Icon } from 'react-native-elements';
import StreamComponent = require('../components/stream/StreamComponent');
import SearchComponent = require('../components/search/SearchComponent');

const Tabs : NavigationContainer = TabNavigator({
    Stream: {
        screen: StreamComponent,
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({ tintColor }) => <Icon name="home" size={35} color={tintColor} />
        }
    },
    Search: {
        screen: SearchComponent,
        navigationOptions: {
            tabBarLabel: 'Search',
            tabBarIcon: ({ tintColor }) => <Icon name="magnifier" size={35} color={tintColor} />
        }
    }
})

export = Tabs;