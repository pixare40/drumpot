import * as React from 'react';
import { StyleSheet, Dimensions, Text, View, WebView, TouchableOpacity, ScrollView } from 'react-native'
import TrackFiendStyles = require('../../TrackFiendStyles');
import TrackFiendIcons = require('../../TrackFiendIcons');
import { Container, Header, Left, Body, Right, Button, Content } from 'native-base'
import { NavigationScreenConfigProps } from 'react-navigation';
import { Icon } from 'react-native-elements';

interface ViewExternalComponentState{
    url: string,
    isLoadingExternalUrl: boolean,
    content: string
}

const styles = StyleSheet.create({
    rootStyles: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
    },

    webViewHeaderStyles: {
        justifyContent: 'flex-start',
    },

    backButtonTextStyles: {
        color: TrackFiendStyles.color.titleTextColor,
        fontWeight: 'normal',
        fontSize: 11,
    },

    backButtonStyles: {
        height: 44,
        width: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },

    scrollViewStyles: {
        flex: 1
    },

    webViewStyles: {
        alignItems: 'stretch',
        alignSelf: 'stretch',
        flex: 1
    }
});

class ViewExternalComponent extends React.Component<{}, ViewExternalComponentState>{
    componentWillUpdate(){
        console.log("component updating");
    }

    render(): JSX.Element | null{
        return (
            <Container>
                <Header style={ {backgroundColor: TrackFiendStyles.color.mainAppColor, height: 44} }>
                    <Left>
                        <TouchableOpacity onPress={ this.goBack }>
                            <View style={ TrackFiendStyles.styles.defaultBackButtonStyles }>
                                <Icon name="chevron-left" color={'white'}
                                type="entypo"/>
                                <Text style={ TrackFiendStyles.styles.defaultBackButtonTextStyles }>
                                    Back
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </Left>
                    <Body/>
                    <Right/>
                </Header>
                {this.renderContent()}
            </Container>
                
        )
    }

    renderContent = () => {
        return this.renderExternalContent();
    }

    renderExternalContent = () =>{
        return(
            <WebView source={ {uri:this.state.url} }
                style={ styles.webViewStyles }
                onLoad={ this.externalContentLoaded }
                />
        )
    }

    renderTrackFiendContent = () =>{
        return(
            <WebView source={{ html: this.state.content} }
                style={ styles.webViewStyles }
                onLoad={ this.externalContentLoaded }
                />
        )
    }

    externalContentLoaded = (e: any)=>{
        //ViewExternalStore.externalContentRendered()
    }

    goBack = () => {
        let myProps = this.props as NavigationScreenConfigProps;
        myProps.navigation.goBack();
    }
}

export = ViewExternalComponent