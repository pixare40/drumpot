import * as React from 'react';
import NewsItem = require('../../models/NewsItem');

interface NewsComponentState{
    newsItem: NewsItem
}

class NewsComponent extends React.Component<{}, NewsComponentState>{
    render(){
        return(
            <div className="container">
                <h1>{ this.state.newsItem.title }</h1>
                <div>
                    { this.state.newsItem.news_story }
                </div>
            </div>
        )
    }
}

export default NewsComponent;