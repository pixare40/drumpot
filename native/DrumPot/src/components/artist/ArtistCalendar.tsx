import * as React from 'react';
import { View, Text } from 'react-native';

interface ArtistCalendarState{

}

class ArtistCalendar extends React.Component<{}, ArtistCalendarState>{
    protected _buildState(props: {}, initialBuild: boolean): ArtistCalendarState{
        return {}
    }

    render(): JSX.Element | null{
        return (
            <View>
                <Text>Calendar component</Text>
            </View>
        )
    }
}

export = ArtistCalendar;