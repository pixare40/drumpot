import * as React from 'react';
import { StyleSheet, View, WebView, Text } from 'react-native';
import NewsItem = require('../../models/NewsItem');
import TrackFiendStyles = require('../../TrackFiendStyles');

interface ExternalVideoComponentProps extends React.Props<any>{
    newsItem: NewsItem
}

const baseUrl = "http://192.168.1.65:4000/show-video/"
const _style = StyleSheet.create({
    rootView: {
        // height: 220,
        // marginBottom: 5,
        // backgroundColor: TrackFiendStyles.color.appBackgroundColour,
        // borderColor: TrackFiendStyles.color.cardBorderColor,
        // borderWidth: 1,
        // borderStyle: 'solid',
    },

    webviewStyle: {
        height: 200,
    },

    titleTextStyles: {
        // fontSize: 12,
        // fontWeight: 'bold',
        // height: 20,
        // paddingRight: 10,
        // paddingLeft: 10
    }
});

class ExternalVideoComponent extends React.Component<ExternalVideoComponentProps, null>{

    render(): JSX.Element | null{
        return (
            <View style={ _style.rootView }>
                <Text style={ _style.titleTextStyles }>
                    { this.props.newsItem.title }
                </Text>
                <WebView scalesPageToFit={ true }
                        javaScriptEnabled={ true }
                        style = { _style.webviewStyle } 
                        source={ {uri:this.renderGetUrl()} } />
            </View>
        )
    }

    renderGetUrl = () => {
        return baseUrl + encodeURIComponent(this.props.newsItem.url_to_story)
    }
}

export = ExternalVideoComponent;