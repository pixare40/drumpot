import * as React from 'react';
import { TouchableWithoutFeedback, Image, Text, View, StyleSheet } from 'react-native';
import { NavigationScreenConfigProps } from 'react-navigation';
import NewsItem = require('../../models/NewsItem')
import TrackFiendStyles = require ('../../TrackFiendStyles')

const _styles = StyleSheet.create({
    mainComponentStyles: {
        position: 'relative',
        height: 100,
        flexDirection: 'row',
        alignItems: 'stretch',
        marginTop: 0,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: 5,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
        borderColor: TrackFiendStyles.color.cardBorderColor,
        borderWidth: 1,
        borderStyle: 'solid',
    },

    storyContainerStyles: {
        height: 100,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignItems: 'stretch',
        flex: 0.3,
        marginBottom: 5,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
    },

    newsImageStyles: {
        height: 100,
        width: 130
    },

    titleTextStyles: {
        textAlign: 'left',
        margin: 5,
        flex: 0.7
    }
});

interface NewsItemProps extends React.Props<any>{
    key: string
    newsItem: NewsItem,
    navigation: any
}

interface NewsItemState{
    isPlaying: boolean
}

class NewsItemComponent extends React.Component<NewsItemProps, NewsItemState>{

    render(): JSX.Element | null{
        return(
            <TouchableWithoutFeedback onPress={ this.onStoryTap } style= { _styles.mainComponentStyles }>
                <View style={ _styles.storyContainerStyles }>
                    <Image source={ {uri:this.props.newsItem.news_item_image} } 
                                style={ _styles.newsImageStyles } 
                                resizeMode={ 'cover' }>
                    </Image>

                    <Text style={ [TrackFiendStyles.styles.defaultTextStyles, 
                                        _styles.titleTextStyles] } 
                                numberOfLines={ 2 }>
                        {this.props.newsItem.title}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    onStoryTap = () =>{
        // if(this.props.newsItem.item_type == "internal_article"){
        //     ViewExternalStore.setContent(this.props.newsItem.news_story);
        //     ViewExternalStore.setIsTrackFiendArticle(true);
        // }

        // ViewExternalStore.setUrlToRender(this.props.newsItem.url_to_story);
        // NavigationStore.navigate('ViewExternal');
    }
}

export = NewsItemComponent;