import * as React from 'react';
import { ScrollView, View } from 'react-native';
import { ListItem } from 'react-native-elements';

class Settings extends React.Component {
  render() {
    return (
      <ScrollView>
        <View>
          <ListItem
            title="Notifications"
          />
          <ListItem
            title="Profile"
          />
          <ListItem
            title="Password"
          />
        </View>
        <View>
          <ListItem
            title="Sign Out"
            rightIcon={{ name: 'cancel' }}
          />
        </View>
      </ScrollView>
    );
  }
}

export default Settings;