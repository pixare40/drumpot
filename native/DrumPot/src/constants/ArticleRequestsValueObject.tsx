import gql from 'graphql-tag';

export const GET_ARTICLES = gql`
query{
    articles{
        id
        title
        body
        url
        summary
        article_type_id
        artists{
            name
        }
    }
}
`