import * as React from 'react';
import { StyleSheet, Dimensions, ScrollView, View } from 'react-native';
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel');
import ArtistTileItemComponent = require('./ArtistTileItemComponent');
import TrackFiendStyles = require('../../TrackFiendStyles');
import { NavigationScreenConfigProps } from 'react-navigation';

interface TopFollowedArtistsState{
    followedArtists: TopArtistInfoModel[]
    isLoadingTopFollowed: boolean,
}

interface TopFollowedArtistsProps extends React.Props<any>{
    navigationProps: NavigationScreenConfigProps
}

const _styles = StyleSheet.create({
    tileContainerStyles: {
        flexDirection: 'row',
        flexWrap: 'wrap' 
    },

    tileItemStyles: {
        width: Dimensions.get('window').width/2 - 5,
        height: 150,
        justifyContent: 'center',
        borderColor: TrackFiendStyles.color.appBackgroundColour,
        borderWidth: 2,
        borderStyle: 'solid',
        marginBottom: 4
    }
})

class TopFollowedArtistsComponent extends React.Component<TopFollowedArtistsProps, TopFollowedArtistsState>{

    render(): JSX.Element|null{
        return(
            <ScrollView>
                <View style={ _styles.tileContainerStyles }>
                    { this.renderTileItem() }
                </View>
            </ScrollView>
        )
    }

    renderTileItem(){
        return(
            this.state.followedArtists.map((artist, index)=>{
                return <ArtistTileItemComponent onArtistTapped={ this.onArtistTapped }
                                            artist={ artist }
                                            styles={ _styles.tileItemStyles }
                                            key={ artist.artist_id }/>
            })
        )
    }

    onArtistTapped = (artist_id: number) =>{
        //ArtistStore.setCurrentViewedArtist(artist_id)
        this.props.navigationProps.navigation.navigate('Artist', {artist_id: artist_id})
    }
}

export = TopFollowedArtistsComponent;