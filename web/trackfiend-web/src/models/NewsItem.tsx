interface NewsItem{
    key: string,
    tags: Array<string>,
    news_item_image: string,
    title: string,
    summary: string,
    news_story: string,
    date_fetched: string,
    date_created: string,
    url_to_story: string,
    item_type: string,
    has_media: boolean
}

export = NewsItem;