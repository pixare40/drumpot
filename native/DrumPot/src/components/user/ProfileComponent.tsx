import * as React from 'react';
import { StyleSheet, View, ActivityIndicator, Text } from 'react-native';
import UserInfoModel = require('../../models/user/UserInfoModel');
import TrackFiendStyles = require('../../TrackFiendStyles');
import TabButtonModel = require('../../models/TabButtonModel');
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel');
import ArtistListItemComponent = require('../artist/ArtistListItemComponent');
import { Container } from 'native-base';
import { Icon } from 'react-native-elements';

interface ProfileComponentState{
    isFetchingUserInfo: boolean
    userInfo: UserInfoModel
}

const _styles = StyleSheet.create({
    rootComponent: {
        flex: 1
    },

    profileComponent: {
        backgroundColor: TrackFiendStyles.color.profileHeaderBackgroundColor,
        height: 120,
        justifyContent: 'center',
        alignItems: 'center'
    },

    profileComponentText: {
        color: TrackFiendStyles.color.profileHeaderTextColor,
        fontWeight: 'bold',
        fontSize: 16
    },

    tabHeaderStyle: {
        backgroundColor: 'white'
    }
})


class ProfileComponent extends React.Component<{}, ProfileComponentState>{

    render(): JSX.Element | null{
        return(
            <Text>
                {"Profile Component"}
            </Text>
        )

        // if(this.state.isFetchingUserInfo){
        //     return(
        //         <View style={ TrackFiendStyles.styles.spinnerContainer }>
        //             <ActivityIndicator color= { TrackFiendStyles.color.mainAppColor } size={ 'large' }/>
        //         </View>
        //     )
        // }
        
        // return (
        //     <Container>
        //         <View style={ _styles.profileComponent }>
        //             <Text style={ _styles.profileComponentText }>
        //                 { this.state.userInfo.firstname } { this.state.userInfo.lastname }
        //             </Text>
        //         </View>
                
        //     </Container>
        // )
    }

    renderArtistList = (item: any, hasFocus?:boolean) => {
        return (
                <ArtistListItemComponent key={ item.artistinfo.name } 
                                artist={ item.artistinfo } 
                                onArtistTapped={ this.goToArtist }/>
        )
    }

    goToArtist = (artistId: number) =>{
    }
}

export = ProfileComponent;