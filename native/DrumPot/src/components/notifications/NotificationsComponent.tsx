import * as React from 'react';
import { View, Text } from 'react-native';

interface NotificationsComponentProps{

}

interface NotificationsComponentState{

}

class NotificationsComponent extends React.Component<NotificationsComponentProps, NotificationsComponentState>{
    render(): JSX.Element | null{
        return (
            <View>
                <Text>
                    Notifications Component
                </Text>
            </View>
        )
    }
}

export = NotificationsComponent;