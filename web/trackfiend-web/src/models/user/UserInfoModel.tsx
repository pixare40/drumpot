import ArtistInfoModel = require('../artists/ArtistInfoModel')

interface UserInfoModel{
    firstname: string,
    lastname: string,
    email: string,
    followed_artists: ArtistInfoModel[]
}

export = UserInfoModel;