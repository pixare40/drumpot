import * as React from 'react';
import { StyleSheet, View, Image, TouchableWithoutFeedback, Text } from 'react-native';
import NewsItemComponent = require('./NewsItemComponent');
import TrackFiendStyles = require ('../../TrackFiendStyles');

const _jumboStyles = StyleSheet.create({
    mainComponentStyles: {
        height: 280,
        marginTop: 5,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: 5,
        flex: 1,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour,
    },

    mainTitleStyle: {
        fontSize: 12,
        fontWeight: 'bold',
        position: 'absolute',
        bottom: 0,
        color: TrackFiendStyles.color.white,
        padding: 15,
        shadowColor: TrackFiendStyles.color.defaultShadowColor,
        backgroundColor: TrackFiendStyles.color.titleTextBackgroundColour
    },

    jumboImageStyle: {
        height: 280
    },

    jumboComponentStyles: {
        flexDirection: 'row',
        alignItems: 'stretch'
    }
});

class JumboComponent extends NewsItemComponent{
    render(): JSX.Element | null{
        return(
            <TouchableWithoutFeedback onPress={ this.onStoryTap }>
                <View style={ _jumboStyles.mainComponentStyles }>
                    <Image source={ {uri: this.props.newsItem.news_item_image} } 
                                resizeMode={ 'cover' }
                                style={ _jumboStyles.jumboImageStyle }>
                    </Image>

                    <Text style={ _jumboStyles.mainTitleStyle }>
                        {this.props.newsItem.title}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

export = JumboComponent;