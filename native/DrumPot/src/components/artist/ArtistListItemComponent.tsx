import * as React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Button } from 'react-native';
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel')
import TrackFiendStyles = require('../../TrackFiendStyles')
import UserInfoModel = require('../../models/user/UserInfoModel')

interface ArtistListItemProps{
    key: string
    artist: ArtistInfoModel
    showTracking?: boolean
    showUntrack?: boolean
    onArtistTapped: (artistId: number) => void
}

const _styles = StyleSheet.create({
    rootViewStyles: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 40,
        marginBottom: 2,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    },

    trackButtonStyles: {
        borderStyle: 'solid',
        borderColor: TrackFiendStyles.color.mainAppColor,
        borderWidth: 1,
        height: 20,
        alignItems: 'center',
        width: 100,
        borderRadius: 5
    },

    trackingTextContainerStyle: {
        width: 100,
        alignItems: 'center'
    },

    trackTextStyle: {
        color: TrackFiendStyles.color.mainAppColor,
        fontSize: 11,
        fontWeight: 'bold',
    },

    artistNameContainerStyles: {
        flex: 1,
        justifyContent: 'center'
    },

    artistNameStyles: {
        fontSize: 11,
    },

    buttonContainerView: {
        marginRight: 10,
        alignItems: 'center',
        flex: 0,
        justifyContent: 'center'
    }
});

interface ArtistListItemComponentState{
    authToken: string,
    followedArtists: UserInfoModel
}

class ArtistListItemComponent extends React.Component<ArtistListItemProps, ArtistListItemComponentState>{

    render(): JSX.Element | null{
        return (
            <View style={ _styles.rootViewStyles }>
                <TouchableOpacity style={ _styles.artistNameContainerStyles } 
                            onPress = { this.onArtistPressed }>
                    <Text style={ _styles.artistNameStyles }>
                        { this.props.artist.name }
                    </Text>
                </TouchableOpacity>
                <View style={ _styles.buttonContainerView }>
                    { this.showTrackingAction() }
                </View>
            </View>
        )
    }

    onArtistPressed = ()=>{
        this.props.onArtistTapped(this.props.artist.id)
    }

    startTrackingArtist = () => {
        // TrackArtistClient.trackArtist(this.props.artist.id, this.state.authToken)
        // .then(()=>{
        //     //UserProfileStore.fetchUserInfo();
        // })
    }

    showTrackingAction = () => {
        // if(LoginStore.getLoggedInStatus() && UserProfileStore.isFollowingArtist(this.props.artist.id)){
        //     return(
        //         <View style={ _styles.trackingTextContainerStyle }>
        //             <Text style={ _styles.trackTextStyle }>
        //                 Tracking
        //             </Text>
        //         </View>
        //     )
        // }

        return(
            <TouchableOpacity style={ _styles.trackButtonStyles } onPress={ this.startTrackingArtist }>
                <Text style={ _styles.trackTextStyle }>
                    Track
                </Text>
            </TouchableOpacity>
        )

    }
}

export = ArtistListItemComponent;