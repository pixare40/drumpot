import { StyleSheet, View, TextInput, Text, Keyboard } from 'react-native';
import * as React from "react";
import TrackFiendStyles = require('../../TrackFiendStyles');

import { 
    Container, 
    Header, 
    Content, 
    Form, 
    Item, 
    Label, 
    Input 
} from 'native-base';
import { Button } from 'react-native-elements';
import { NavigationScreenConfigProps } from 'react-navigation';

interface LoginState{
    loggedIn: boolean,
    username: string,
    password: string,
    firstname: string,
    lastname: string
}

class LoginComponent extends React.Component<{}, LoginState>{

    render() : JSX.Element | null{
        return (
            <View style={ {flex:1,
                alignSelf: 'stretch',
                alignItems: 'center',
                justifyContent: 'center'} }>
                <View style={ {width:300} }>
                    <Form>
                        <Item>
                            <Input placeholder={ 'Username' } onChangeText = { this.setUserName } />
                        </Item>
                        <Item>
                            <Input placeholder={ 'Password' } onChangeText = { this.setPassword } 
                                secureTextEntry={ true } />
                        </Item>
                    </Form>
                    <Button onPress={ this.onLoginPressed } title='Login' />
                    {/* <LoginButton publishPermission={['public_profile']}
                        readPermissions={['email','public_profile']}
                        onLoginFinished={
                            (error, result) =>{
                                if(error){
                                    alert('Login failed with error: ' + result.error);
                                }else if(result.isCancelled){
                                    alert('login was cancelled');
                                } else{
                                    this.loginWithFacebook();
                                }
                            }
                        }
                        onLogoutFinishes={()=>{
                            alert('User logged out');
                        }}/> */}
                </View>
            </View>
        )
    }

    setUserName = (userName: string) => {
        this.setState(Object.assign({username: userName}));
    }

    setPassword = (passWord: string) =>{
        this.setState(Object.assign({password: passWord}));
    }

    loginWithFacebook = () =>{
        // let accessToken;
        // AccessToken.getCurrentAccessToken().then((data)=>{
        //     accessToken = data.accessToken;
        //     console.log(accessToken.toString());
        // })

        // let profileRequest = new GraphRequest(
        //     '/me',
        //     {
        //         accessToken: accessToken,
        //         parameters: {
        //           fields: {
        //             string: 'email,name,first_name,middle_name,last_name,picture'
        //           }
        //         }
        //       },
        //     this.loginResponseCallback,
        // );

        // new GraphRequestManager().addRequest(profileRequest).start();
    }

    loginResponseCallback = (error?: object, result?: object) =>{
        if (error) {
            alert('Error fetching data: ' + error.toString());
        } else {
        }
        let email = result['email'];
        let userName = result['first_name'];
        console.log(result);
        //LoginStore.facebookLogin(email);
        let profilePicUrl = result['picture']['data']['url'];
        //LoginStore.setUserProfilePicture(profilePicUrl);
        //LoginStore.setLoginUsername(userName);
        Keyboard.dismiss()
    }

    componentDidUpdate(){
        if(this.state.loggedIn){
            //UserProfileStore.fetchUserInfo();
            let myProps = this.props as NavigationScreenConfigProps;
            myProps.navigation.navigate('Tabs');
        }
    }

    onLoginPressed = ()=> {
        //LoginStore.login(this.state.username, this.state.password);
        Keyboard.dismiss();
    }

    onRegistrationTapped = () =>{
    }

    onLoginCompleted = (error: any, response: any)=>{

    }
}

export = LoginComponent;