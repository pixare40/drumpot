
interface TabButtonModel{
    title: string,
    iconPath?: string,
    viewBox?: string,
    isSelected: boolean,
    height?: number,
    width?: number,
    showIcon: boolean,
    textContent?: string,
    menuKey: string,
}

export = TabButtonModel;