import * as React from 'react';
import { ApolloProvider } from 'react-apollo';

import { client } from '../graphql';

export default function WithProvider(WrappedComponent) {
    return class Cp extends React.Component {
        render() {
            return (
                <ApolloProvider client={client}>
                    <WrappedComponent {...this.props} />
                </ApolloProvider>
            )
        }
    }
}