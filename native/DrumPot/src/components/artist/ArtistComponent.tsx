import * as React from 'react';
import { Image, StyleSheet, View, Text, ActivityIndicator, TouchableOpacity } from 'react-native';
import TrackFiendStyles = require('../../TrackFiendStyles');
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel');
import ArtistInfoModel = require('../../models/artists/ArtistInfoModel');
import ArtistNewsComponent = require('../news/ArtistNewsComponent');
import ArtistCalendar = require('./ArtistCalendar');
import TrackFiendIcons = require('../../TrackFiendIcons');
import { NavigationScreenConfigProps } from 'react-navigation';
import { Container, Left, Right, Body, Header } from 'native-base';
import { TabNavigator, StackNavigator, NavigationContainer, TabBarBottom } from 'react-navigation';
import ViewExternalComponent = require('../view_external/ViewExternalComponent');

import { Icon } from 'react-native-elements';

interface ArtistComponentProps{
    artist_id: number
}

interface ArtistComponentState{
    artistInfo: ArtistInfoModel
}

const _styles = StyleSheet.create({
    rootComponentStyles: {
        flex: 1,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    },

    artistHeader: {
        flexDirection: 'row',
        padding: 10
    },

    artistImageStyle: {
        borderRadius: 25,
        height: 50,
        width: 50
    },

    artistHeaderTextContainer: {
        paddingLeft: 30
    },

    artistTrackersStyles: {
        fontSize: 12
    },

    backButtonTextStyles: {
        color: TrackFiendStyles.color.titleTextColor,
        fontWeight: 'normal',
        fontSize: 11,
    },

    backButtonStyles: {
        height: 44,
        width: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },
})

class ArtistComponent extends React.Component<{}, ArtistComponentState>{

    private _props: NavigationScreenConfigProps;
    private ArtistTabs : NavigationContainer = TabNavigator({
        ArtistNews: {
            screen: ArtistNewsComponent,
            navigationOptions: {
                tabBarLabel: 'News',
                tabBarIcon: ({ tintColor }) => <Icon name="ios-paper-outline" type="ionicon" size={18} color={tintColor} />
            }
        },
        ArtistCalendar: {
            screen: ArtistCalendar,
            navigationOptions: {
                tabBarLabel: 'Calendar',
                tabBarIcon: ({ tintColor }) => <Icon name="calendar" type="evilicon" size={25} color={tintColor} />
            }
        } 
    },{
        tabBarComponent: TabBarBottom,
        swipeEnabled: false,
        animationEnabled: false,
        tabBarOptions:{
            activeTintColor: TrackFiendStyles.color.mainAppColor,
            showLabel: false,
            style: {
                height: 44
            }
        }
        }
    );

    private ArtistStack : NavigationContainer = StackNavigator({
            Tabs: {
                screen: this.ArtistTabs
            },
        }, {
            mode: 'card',
            headerMode: 'none',
            initialRouteName : 'Tabs',
    });
    
    constructor(props: {}){
        super(props)
        this._props = props as NavigationScreenConfigProps
    }

    render(): JSX.Element | null{
        return(
            this.renderView()
        )
    }

    renderView(){
        if(this.state.artistInfo)
        {
            return(
                <Container>
                    <Header style={ {backgroundColor: TrackFiendStyles.color.mainAppColor, height: 44} }>
                        <Left>
                            <TouchableOpacity onPress={ this.goBack }>
                                <View style={ _styles.backButtonStyles }>
                                    <Icon name="chevron-left" color={'white'}
                                    type="entypo"/>
                                    <Text style={ _styles.backButtonTextStyles }>
                                        Back
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </Left>
                        <Body/>
                        <Right/>
                    </Header>
                    <View style={ _styles.artistHeader }>
                        { this.renderArtistImage() }
                        <View style={ _styles.artistHeaderTextContainer }>
                            <Text>
                                { this.state.artistInfo.name }
                            </Text>
                            <Text style={ _styles.artistTrackersStyles }>
                               {this.renderArtistTrackerText()}
                            </Text>
                        </View>
                    </View>
                    <this.ArtistStack/>
                </Container>
            )
        }

        return (
            <View style={ TrackFiendStyles.styles.spinnerContainer }>
                <ActivityIndicator color={ TrackFiendStyles.color.mainAppColor } 
                                size={ 'large' }/>
            </View>
        
        )
    }

    renderArtistTrackerText = () => {
        if(this.state.artistInfo.followers == 1){
            return this.state.artistInfo.followers + " Tracker"
        }
        else{
            return this.state.artistInfo.followers + " Trackers"
        }
    }

    renderArtistImage = () =>{
        if(this.state.artistInfo.artist_image_url){
            return(
                <Image source={ {uri:this.state.artistInfo.artist_image_url || TrackFiendIcons.images.personPlaceholder.url} }
                            style={ _styles.artistImageStyle }
                            resizeMode={ 'cover' } />
            )
        }else{
            return(
                <Image source={ {uri:TrackFiendIcons.images.personPlaceholder.url || TrackFiendIcons.images.personPlaceholder.url} }
                            style={ _styles.artistImageStyle }
                            resizeMode={ 'cover' } />
            )
        }
    }

    goBack = () =>{
        let myProps = this.props as NavigationScreenConfigProps;
        myProps.navigation.goBack();
    }
}

export = ArtistComponent;