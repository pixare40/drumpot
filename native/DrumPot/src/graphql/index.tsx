import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApiHost }  from '../NetworkConfiguration';

const cache = new InMemoryCache();

const link = new HttpLink({
    uri: ApiHost
})

export const client = new ApolloClient({
    cache: cache,
    link: link
})

