//Shared styles in the TrackFiend App

import { StyleSheet } from 'react-native'

class TrackFiendStyles{
    static color = {
        mainAppColor: '#e25a57',
        manhattan: '#f2c49c',
        white: 'white',
        titleTextBackgroundColour: 'rgba(226, 90, 87, 0.72)',
        titleTextColor: 'white',
        textColor: 'black',
        defaultShadowColor: 'black',
        cardBackgroundColor: '',
        appBackgroundColour: 'white',
        defaultTabDeselectedColor: 'white',
        facebookColor: '#3b5998',
        profileHeaderBackgroundColor: '#ec5e2f',
        profileHeaderTextColor: 'white',
        listBackgroundColour: '#efefef',
        cardBorderColor: '#F5F5F5',
        twitterIconColor: '#00aced'
    };

    static styles = StyleSheet.create({
        headerWithStatusBar: {
            paddingTop: 22
        },

        header: {
            backgroundColor: TrackFiendStyles.color.mainAppColor,
            flexDirection: 'row',
            height: 44,
            alignItems: 'center',
            justifyContent: 'center',
            position: 'relative'
        },

        container: {
            flex: 1,
            backgroundColor: TrackFiendStyles.color.white,
            alignSelf: 'stretch'
        },

        defaultButtonStyles: {
            borderRadius: 5,
            borderColor: TrackFiendStyles.color.mainAppColor,
            backgroundColor: TrackFiendStyles.color.white,
        },

        defaultButtonTextStyles: {
            color: TrackFiendStyles.color.mainAppColor,
            fontSize: 12
        },

        defaultTextStyles : {
            fontSize: 11,
            color: TrackFiendStyles.color.textColor,
            fontFamily: "Montserrat-Regular"
        },

        defaultRoundedButtonStyles: {
            height: 40,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: TrackFiendStyles.color.mainAppColor,
            borderRadius: 20,
            margin: 2,
            alignSelf: 'stretch'
        },

        facebookButtonStyles:  {
            backgroundColor: TrackFiendStyles.color.facebookColor
        },

        spinnerContainer: {
            flex: 1,
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'center'
        },

        defaultBackButtonContainerHeaderStyles: {
            justifyContent: 'flex-start',
        },

        defaultBackButtonTextStyles: {
            color: TrackFiendStyles.color.titleTextColor,
            fontWeight: 'normal',
            fontSize: 11,
        },

        defaultBackButtonStyles: {
            height: 44,
            width: 50,
            flexDirection: 'row',
            alignItems: 'center'
        }
    });
}

export = TrackFiendStyles;