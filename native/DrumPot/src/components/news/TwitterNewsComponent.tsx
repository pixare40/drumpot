import * as React from 'react';
import NewsItemComponent = require('./NewsItemComponent');
import TweetStatusItem  = require('../../models/TweetStatusItem');
import NewsItem = require ('../../models/NewsItem');
import { default as RNVideo, VideoInfo, VideoBufferInfo } from 'react-native-video';
import { Dimensions, View, StyleSheet, Image, Text, TouchableWithoutFeedback } from 'react-native';
import TrackFiendStyles = require('../../TrackFiendStyles');
import TrackFiendIcons = require('../../TrackFiendIcons');
import { Icon } from 'react-native-elements';
import TwitterUserKeysValueObject = require('../../value_objects/TwitterUserKeysValueObject');
import { Container, Content } from 'native-base';

const _styles= StyleSheet.create({
    tweetImageStyle: {
        height: 200
    },

    videoStyles: {
        height: 200,
        maxHeight: 200
    },

    tweetTextStyles: {
        fontSize: 11,
        flex: 1
    },

    tweetContainerStyles: {
        marginTop:0,
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: 'white',
        borderColor: TrackFiendStyles.color.cardBorderColor,
        borderWidth: 1,
        borderStyle: 'solid',
    },

    tweetDescriptionStyles: {
        fontSize: 11
    },

    tweetMediaContainer: {
        height: 283,
        marginBottom: 5,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    },

    tweetTextContainer: {
        height: 98,
        padding: 5,
        flex: 1,
        flexDirection: 'row',
        marginBottom: 5,
        backgroundColor: TrackFiendStyles.color.appBackgroundColour
    },

    tweetDescriptionContainer: {
        flexDirection: 'row',
        height: 85,
        flex: 1,
        padding: 5,
    },

    userTwitterName: {
        fontSize: 12,
        fontWeight: 'bold',
        flex: 1,
        width: Dimensions.get('window').width - 82,
    },

    userTwitterUserHandle: {
        fontSize: 10
    },

    userNameContainerStyles: {
        flexDirection: 'row'
    },

    twitterIconStyles: {
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    },

    profilePictureContainer: {
        height: 85,
        width: 40
    },

    profilePictureStyles: {
        height: 30,
        width: 30,
        borderRadius: 4,
    }
});

class TwitterNewsComponent extends NewsItemComponent{
    private _videoComponent: RNVideo;
    private _isPlaying: boolean = false;

    render(): JSX.Element | null{
        return(
            <TouchableWithoutFeedback style={ _styles.tweetContainerStyles }>
                { this.renderTwitterItem() }
            </TouchableWithoutFeedback>
        )
    }

    playVideo = () =>{
        if(this.state.isPlaying){
            this.setState({isPlaying: false});
        }
        else{
            this.setState({isPlaying: true});
        }
    }

    renderTwitterItem = () =>{
        let currentProps = this.props as any
        let twitter_status_item = currentProps.newsItem as TweetStatusItem
        if(twitter_status_item.has_media!= null && twitter_status_item.has_media 
            && twitter_status_item.media_type == 'photo'){
                return(
                    <View style={ _styles.tweetMediaContainer }>
                        <Image source={ {uri:twitter_status_item.media_url_https} }
                                    resizeMode={ 'contain' } 
                                    style={ _styles.tweetImageStyle }/>
                        <View style={ _styles.tweetDescriptionContainer }>
                            <View style={ _styles.profilePictureContainer }>
                                <Image source={ {uri:twitter_status_item
                                        .user[TwitterUserKeysValueObject.PROFILE_IMAGE] || TrackFiendIcons.images.personPlaceholder.url}} 
                                        style={ _styles.profilePictureStyles }/>
                            </View>
                            <View style={ {flex: 1} }>
                                <View style = { _styles.userNameContainerStyles }>
                                    <Text style={ _styles.userTwitterName }>
                                        { twitter_status_item.user['name'] }
                                    </Text>
                                    <Icon name="twitter" 
                                        color={ TrackFiendStyles.color.twitterIconColor }
                                        iconStyle={ _styles.twitterIconStyles }
                                        type="entypo"
                                        size={ 18 }/>
                                </View>
                                <Text style={ _styles.userTwitterUserHandle }>
                                    { '@'+twitter_status_item.user['screen_name'] }
                                </Text>
                                <Text style={ _styles.tweetDescriptionStyles }
                                            numberOfLines= { 3 }>
                                    { twitter_status_item.tweet_text.replace('&amp;', '&') }
                                </Text>
                            </View>
                        </View>
                    </View>
                )
            }
        else if(twitter_status_item.has_media!= null && twitter_status_item.has_media 
            && twitter_status_item.media_type == 'video'){
                return(
                    <TouchableWithoutFeedback style={ _styles.tweetMediaContainer }
                                onPress={ this.playVideo }>
                        <View style={ {flex: 1, backgroundColor: 'white', marginBottom: 5} }>
                            <View style={ {height: 200} }>
                                <RNVideo source={ {uri:twitter_status_item.media_url_https} }
                                            style={ _styles.videoStyles }
                                            showControls={ false }
                                            resizeMode ={ 'cover' }
                                            paused={ !this.state.isPlaying }
                                            ref = { (video: RNVideo)=>{ this._videoComponent = video } }/>
                            </View>
                            <View style={ _styles.tweetDescriptionContainer }>
                                <View style={ _styles.profilePictureContainer }>
                                    <Image source={ {uri:twitter_status_item
                                            .user[TwitterUserKeysValueObject.PROFILE_IMAGE] || TrackFiendIcons.images.personPlaceholder.url}} 
                                            style={ _styles.profilePictureStyles }/>
                                </View>
                                <View style={ {flex: 1} }>
                                    <View style = { _styles.userNameContainerStyles }>
                                        <Text style={ _styles.userTwitterName }>
                                            { twitter_status_item.user['name'] }
                                        </Text>
                                        <Icon name="twitter" 
                                            color={ TrackFiendStyles.color.twitterIconColor }
                                            iconStyle={ _styles.twitterIconStyles }
                                            type="entypo"
                                            size={ 18 }/>
                                    </View>
                                    <Text style={ _styles.userTwitterUserHandle }>
                                        { '@'+twitter_status_item.user['screen_name'] }
                                    </Text>
                                    <Text style={ _styles.tweetDescriptionStyles }
                                                numberOfLines= { 3 }>
                                        { twitter_status_item.tweet_text.replace('&amp;', '&') }
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                )
            }
        else{
            return (
                <View style={ _styles.tweetTextContainer }>
                    <View style={ _styles.profilePictureContainer }>
                                <Image source={ {uri:twitter_status_item
                                        .user[TwitterUserKeysValueObject.PROFILE_IMAGE] || TrackFiendIcons.images.personPlaceholder.url}} 
                                        style={ _styles.profilePictureStyles }/>
                    </View>
                    <View style={ {flex: 1} }>
                        <View style = { _styles.userNameContainerStyles }>
                            <Text style={ _styles.userTwitterName }>
                                { twitter_status_item.user['name'] }
                            </Text>
                            <Icon name="twitter" 
                                color={ TrackFiendStyles.color.twitterIconColor }
                                iconStyle={ _styles.twitterIconStyles }
                                type="entypo"
                                size={ 18 }/>
                        </View>
                        <Text style={ _styles.userTwitterUserHandle }>
                            { '@'+twitter_status_item.user['screen_name'] }
                        </Text>
                        <Text style={ _styles.tweetTextStyles }
                                    numberOfLines= { 3 }>
                            { twitter_status_item.tweet_text.replace('&amp;', '&') }
                        </Text>
                    </View>
                </View>
            )
        }
    }
}

export = TwitterNewsComponent;