import NewsItem = require ('../models/NewsItem')

interface NewsState{
    news: Array<NewsItem>,
    isLoadingNews: boolean
}

export = NewsState;