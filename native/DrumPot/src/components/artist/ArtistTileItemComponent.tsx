import * as React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import TopArtistInfoModel = require('../../models/artists/TopArtistInfoModel')
import TrackFiendStyles = require ('../../TrackFiendStyles');
import TrackFiendIcons = require('../../TrackFiendIcons');

interface ArtistTileItemProps{
    onArtistTapped: (artist_id: number) => void
    artist: TopArtistInfoModel,
    styles: any,
    key: number
}

const _styles = StyleSheet.create({
    imageStyles: {
        height: 150
    },

    textStyles: {
        fontSize: 11,
        position: 'absolute',
        bottom: 0,
        color: TrackFiendStyles.color.titleTextColor,
        padding: 5,
        shadowColor: TrackFiendStyles.color.defaultShadowColor,
        backgroundColor: TrackFiendStyles.color.titleTextBackgroundColour
    }
});

class ArtistTileItemComponent extends React.Component<ArtistTileItemProps, null>{

    render(): JSX.Element | null{
        return(
            <TouchableOpacity onPress={ this.goToArtist }>
                <View style={ this.props.styles } >
                    {this.renderArtistImage()}
                    <Text style={ _styles.textStyles }>
                        { this.props.artist.name }
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    goToArtist = ()=>{
        this.props.onArtistTapped(this.props.artist.artist_id)
    }

    renderArtistImage(){
        if(this.props.artist.artist_image_url){
            return (
                <Image source={ {uri:this.props.artist.artist_image_url || TrackFiendIcons.images.personPlaceholder.url} } 
                            resizeMode={ 'cover' }
                            style={ _styles.imageStyles }/>
            )
        }
        else{
            return (
                <Image source={ {uri: TrackFiendIcons.images.personPlaceholder.url} } 
                            resizeMode={ 'cover' }
                            style={ _styles.imageStyles }/>
            )
        }
    }
}

export = ArtistTileItemComponent;