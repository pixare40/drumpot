import * as React from 'react';
import { AppRegistry } from 'react-native';
import { createBottomTabNavigator, createStackNavigator, NavigationContainer, TabBarBottom, createAppContainer } from 'react-navigation';
import { Icon } from 'react-native-elements'
import StreamComponent = require('./components/stream/StreamComponent');
import SearchComponent = require('./components/search/SearchComponent');
import NotificationsComponent = require('./components/notifications/NotificationsComponent');
import ProfileComponent = require('./components/user/ProfileComponent');
import TrendingComponent = require('./components/trending/TrendingComponent');
import Settings from './components/settings/Settings';
import TrackFiendStyles = require('./TrackFiendStyles');
import LoginScreenComponent = require('./components/authentication/LoginScreenComponent');
import ViewExternalComponent = require('./components/view_external/ViewExternalComponent');
import ArtistComponent = require('./components/artist/ArtistComponent');
import { createHttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApiHost } from './NetworkConfiguration';
import { ApolloProvider } from 'react-apollo';
import { client } from './graphql';

export default class DrumPot extends React.Component<object, object> {

    private Tabs : NavigationContainer = createBottomTabNavigator({
        Stream: {
            screen: StreamComponent,
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: ({ tintColor }) => <Icon name="home" size={25} color={tintColor} />
            }
        },
        Search: {
            screen: SearchComponent,
            navigationOptions: {
                tabBarLabel: 'Search',
                tabBarIcon: ({ tintColor }) => <Icon name="search" size={25} color={tintColor} />
            }
        },
        Trending: {
            screen: TrendingComponent,
            navigationOptions: {
                tabBarLabel: 'Trending',
                tabBarIcon: ({tintColor}) => <Icon name="fire" type="simple-line-icon" size={25} color={tintColor}/>
            }
        },
        notifications: {
            screen: NotificationsComponent,
            navigationOptions: {
                tabBarLabel: 'Notifications',
                tabBarIcon: ({tintColor}) => <Icon name="notifications" size={25} color={tintColor}/>
            }
        },
        Profile: {
            screen: ProfileComponent,
            navigationOptions: {
                tabBarLabel: 'Profile',
                tabBarIcon: ({tintColor}) => <Icon name="user" type="font-awesome" size={25} color={tintColor}/>
            }
        }
    },{
        tabBarComponent: TabBarBottom,
        tabBarPosition : 'bottom',
        swipeEnabled: false,
        animationEnabled: false,
        tabBarOptions:{
            activeTintColor: TrackFiendStyles.color.mainAppColor,
            showLabel: false,
            style: {
                height: 44
            }
        }
        }
    );

    private httpLink = createHttpLink({
        uri: ApiHost,
      })
      
    private cache = new InMemoryCache();
      
    private client = new ApolloClient({
        link: this.httpLink,
        cache: this.cache
      });

    private AppNavigator : NavigationContainer  = createStackNavigator({
        Tabs: {
            screen: this.Tabs
        },
        Login: {
            screen: LoginScreenComponent
        },
        ViewExternal: {
            screen: ViewExternalComponent
        },
        Artist: {
            screen: ArtistComponent
        }
        }, {
        mode: 'card',
        headerMode: 'none',
        initialRouteName : 'Tabs',
    });

    private AppContainer = createAppContainer(this.AppNavigator);

    render(): JSX.Element {
        return (
            <ApolloProvider client={client}>
                <this.AppContainer />
            </ApolloProvider>
        );
    }
}

AppRegistry.registerComponent('DrumPot', () => DrumPot);
