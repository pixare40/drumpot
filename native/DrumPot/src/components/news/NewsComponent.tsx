import * as React from 'react';
import { StyleSheet, FlatList, View, ActivityIndicator, ListRenderItemInfo, TouchableOpacity, Text } from 'react-native';
import { NavigationScreenConfigProps } from 'react-navigation';
import TrackFiendStyles = require ('../../TrackFiendStyles');
import NewsItem = require ('../../models/NewsItem');
import NewsItemComponent = require('./NewsItemComponent');
import JumboComponent = require('./JumboComponent');
import TwitterNewsComponent = require('./TwitterNewsComponent');
import NewsState = require('../../state/NewsState');
import ExternalVideoComponent = require('../video/ExternalVideoComponent');
import { Container, Header, Content, Left, Body, Right } from 'native-base';
import { Icon } from 'react-native-elements';

const _newsComponentStyles = StyleSheet.create({
    activityStyles: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },

    spinnerContainer: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center'
    },

    listScroll: {
        flexDirection: 'column',
        alignSelf: 'stretch',
        backgroundColor: TrackFiendStyles.color.listBackgroundColour,
        paddingTop: 5
    },

    rootView: {
        flexDirection: 'column',
        alignSelf: 'stretch'
    },

    backButtonTextStyles: {
        color: TrackFiendStyles.color.titleTextColor,
        fontWeight: 'normal',
        fontSize: 11,
    },

    backButtonStyles: {
        height: 44,
        width: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },
});

interface NewsComponentProps extends React.Props<any>{
    navigation: NavigationScreenConfigProps
}

class NewsComponent extends React.Component<NewsComponentProps, NewsState>{

    onFetchNewsError = ()=>{
    }

    render(): JSX.Element | null{
        return (
                this.renderNews() 
        );
    }

    renderNews = ()=>{
        if(this.state.isLoadingNews){
            return  <View style={ _newsComponentStyles.spinnerContainer }>
                        <ActivityIndicator color={ TrackFiendStyles.color.mainAppColor } size={ 'large' } style={ _newsComponentStyles.activityStyles }/>
                    </View>
        }
        else{
            return(
                <View>
                    <FlatList data={ this.state.news } renderItem={ this.renderNewsItems } />
                </View>
            )
        }
    }

    renderNewsItems = (item: ListRenderItemInfo<NewsItem>)=>{
        if(item.item.item_type == 'twitter_post'){
            return (<TwitterNewsComponent key={ item.item.title } 
                                            newsItem={ item.item } navigation={ this.props.navigation }/>)
        }
        else if(item.item.item_type == 'external_video'){
            return(<ExternalVideoComponent newsItem={ item.item }/>)
        }
        else{
            return <NewsItemComponent key={ item.item.title } 
                                        newsItem= { item.item} navigation={ this.props.navigation }/>
        }
    }
}

export = NewsComponent;