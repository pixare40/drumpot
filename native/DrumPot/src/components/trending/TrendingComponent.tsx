import * as React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

interface TrendingComponentProps{

}

interface TrendingComponentState{

}

class TrendingComponent extends React.Component<TrendingComponentProps, TrendingComponentState>{
    render(): JSX.Element | null{
        return(
            <View>
                <Text> Trending Screen </Text>
            </View>
        )
    }
    
}

export = TrendingComponent; 