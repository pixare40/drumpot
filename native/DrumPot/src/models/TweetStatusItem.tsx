import NewsItem = require('./NewsItem')

interface TweetStatusItem extends NewsItem{
    tweet_text: string,
    media_url: string,
    media_url_https: string,
    media_type: string,
    media_sizes: {},
    user: {[key: string]: string},
}

export = TweetStatusItem