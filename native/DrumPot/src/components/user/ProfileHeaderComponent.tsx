import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Thumbnail } from 'native-base'
import TrackFiendStyles = require('../../TrackFiendStyles');

interface ProfileHeaderComponentProps extends React.Props<any>{
    userFirstName: string
    profilePictureUrl: string
}

interface ProfileHeaderComponentState{

}

const styles = StyleSheet.create({
    rootComponent: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: TrackFiendStyles.color.mainAppColor,
    },

    textContainerComponent: {
        height: 50
    },

    welcomeTextColor: {
        color: TrackFiendStyles.color.white
    },

    thumbnailContainer: {
        borderWidth: 6,
        borderColor: TrackFiendStyles.color.mainAppColor
    },
})

class ProfileHeaderComponent extends React.Component<ProfileHeaderComponentProps, ProfileHeaderComponentState>{
    render(): JSX.Element | null{
        return(
            <View style={ styles.rootComponent }>
                <View style={ styles.textContainerComponent }>
                    <Text style={ styles.welcomeTextColor }> Hi {this.props.userFirstName}</Text>
                </View>
                <View style={ styles.thumbnailContainer }>
                    <Thumbnail size={30} source={{uri:this.props.profilePictureUrl}} />
                </View>
            </View>
        )
    }
}

export = ProfileHeaderComponent;