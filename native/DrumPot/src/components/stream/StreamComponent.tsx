import * as React from 'react';
import { View, StyleSheet, FlatList, ListRenderItemInfo, ActivityIndicator, Text } from 'react-native';
import NewsItem = require('../../models/NewsItem')
import { NavigationScreenConfigProps } from 'react-navigation';
import JumboComponent = require('../news/JumboComponent');
import TwitterNewsComponent = require('../news/TwitterNewsComponent');
import ExternalVideoComponent = require('../video/ExternalVideoComponent');
import NewsItemComponent = require('../news/NewsItemComponent');
import ProfileHeaderComponent = require('../user/ProfileHeaderComponent');
import { Container, Content, Card, CardItem, Body } from 'native-base';
import TrackFiendStyles = require('../../TrackFiendStyles');
import { GET_ARTICLES } from '../../constants/ArticleRequestsValueObject';
import { graphql, Query } from 'react-apollo';
import { Divider } from 'react-native-elements';

interface StreamComponentState {
    news: Array<NewsItem>,
    isLoadingNews: boolean,
}

const styles = StyleSheet.create({
    listStyle: {
    }
})

class StreamComponent extends React.Component<{}, StreamComponentState>{

    onFetchNewsError = () => {
        let myProps = this.props as NavigationScreenConfigProps;
        myProps.navigation.navigate('Login');
    }

    render() {
        console.log("This is the props", this.props)
        console.log("I will be awesome");
        return (
            <Container>
                <Content>
                    <Query query={GET_ARTICLES}>
                        {({ loading, error, data }) => {
                            if (loading) return <Text> {"Loading"} </Text>
                            if (error) {
                                console.error("Errors", error)
                                return <Text> {"Errors"} </Text>
                            }

                            console.log("Data =>", data);
                            return (
                                <FlatList data={data.articles} renderItem={this.renderNewsItems} keyExtractor={this.keyExtractor}></FlatList>
                            )
                        }
                        }
                    </Query>
                </Content>
            </Container>
        )
    }

    renderNewsItems = (item: ListRenderItemInfo<NewsItem>) => {
        return (
            <Card>
                <CardItem>
                    <View>
                        <Text style={{ fontWeight: "bold" }}>{item.item.title}</Text>
                        <Text>{item.item.summary}</Text>
                    </View>
                </CardItem>
            </Card>
        )
    }

    keyExtractor = (item: NewsItem) => {
        return (
            item.id
        )
    }
}

export = StreamComponent;